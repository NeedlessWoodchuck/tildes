# Tildes

This is the code behind [Tildes](https://tildes.net), a non-profit community site. The official repository is located on GitLab at https://gitlab.com/tildes/tildes

For general information about Tildes and its goals, please see [the announcement blog post](https://blog.tildes.net/announcing-tildes) and [the Tildes Docs site](https://docs.tildes.net).

## Issue tracker / plans

Known issues and plans for upcoming changes are tracked on GitLab: https://gitlab.com/tildes/tildes/issues

The "board" view is useful as an overview: https://gitlab.com/tildes/tildes/boards

## Contributing to Tildes development

Please see [the Contributing doc](CONTRIBUTING.md) for more detailed information about setting up a development version of Tildes and how to contribute to development.
